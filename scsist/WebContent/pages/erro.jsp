<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="model.objects.Usuario"%>
<%@page import="java.util.Iterator"%>


<%
	Usuario u = (Usuario) session.getAttribute("usuario");
	String erro = (String) session.getAttribute("erro");
	if( (u != null) && (u.getNivel().toString().compareTo("GESTOR") == 0) ){
		String nome = u.getNome();
		String login = (String)u.getLogin(); 
		String sala;
%>


<!DOCTYPE html>
<html>
	<head>

		<title>Gestor</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script type="text/javascript" src="../scripts/ajax2.js"></script>
		<script type="text/javascript" src="../scripts/jquery.js" type="text/javascript"></script>
		<script type="text/javascript" src="../scripts/jquery-ui.js"></script>
		<script type="text/javascript" src="../scripts/jquery.validate.js" type="text/javascript"></script>
		<script type="text/javascript" src="../scripts/jquery-1.6.min.js"></script>
		<script type="text/javascript" src="../scripts/jquery.reveal.js"></script>
		<link type="text/css" rel="stylesheet" media="all" href="../styles/style.css"/>
		<link type="text/css" rel="stylesheet" media="all" href="../styles/jquery-ui.css"/>
        <link href="../styles/liteaccordion.css" rel="stylesheet" />
        <script src="../scripts/jquery.min.js"></script>
        <script src="../scripts/jquery.easing.1.3.js"></script>
        <script src="../scripts/liteaccordion.jquery.js"></script>
        <style>
        	fieldset{
				border-radius: 10px;
				border-width: thick;
				border-style: outset;
				padding: 20px;
				width: 500px;
				background: #EFEFEF;
			}
			input[type=button]{
				color: #FFF;
    			background: rgba(2,33,46,0.91);
    			padding: 3px 13px;
    			display: inline-block;
    			font-size: 14px;
   				clear: both;
    			font-weight: bold;
    			cursor: pointer;
    			box-shadow: 10px 10px 5px #888888;
    			border-radius: 10px;
			}
			
			input[type=button]:hover{
    			text-decoration: none;
    			cursor: pointer;
    			color: #FFF;
    			background:#5b829d;
			}
			.reveal-modal {
				visibility: hidden;
				top: -80px; 
				left: 50%;
				margin-left: -200px;
				margin-top: -10%; 
				width: 800px;
				height: 400px;
				background: #eee url(../images/modal-gloss.png) no-repeat -200px -80px;
				position: absolute;
				z-index: 101;
				padding: 30px 40px 34px;
				-moz-border-radius: 5px;
				-webkit-border-radius: 5px;
				border-radius: 20px;
				-moz-box-shadow: 0 0 10px rgba(0,0,0,.4);
				-webkit-box-shadow: 0 0 10px rgba(0,0,0,.4);
				-box-shadow: 0 0 10px rgba(0,0,0,.4);
				border-width: thick;
				border-style: outset;
			}
        </style>
	</head>

	<body>
	<div id="geral">

		<div id="corpo">	
			<div id="menuc">
					<div id="left-side">
						<img src="../images/logo4.png">
					</div>
					<div id="right-side">
						<img src="../images/avatar.gif" alt="user icon" />&ensp;
							<a href="#" class="first"><%=u.getNome()%></a>&ensp;
							<a href="#">Alterar dados</a>&ensp;
							<a href="../EncerraAcesso">Sair</a> &emsp;
					</div>
			</div>
		</div>		
		<div id="conteudo">
			
			<div class="content" style="margin-left:150px;">	
				<div align="center">
					<h1><%=erro%></h1>
					<input type="button" value="Voltar" onClick="location.href=''">
        		</div>
        	</div>
        
		</div>
	
	</div>
			
</html>

<%
}
else{
		response.sendRedirect("../../");
}


%>