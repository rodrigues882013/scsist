package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.SalaDAO;
import model.objects.Sala;

/**
 * Servlet implementation class ListaSala
 */
public class ListaSala extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListaSala() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try{
			String numero = (String)request.getParameter("numero");
			Sala sala = new Sala();
			sala = SalaDAO.selectByID(Integer.parseInt(numero));
			session.setAttribute("sala", sala);
			response.sendRedirect("pages/gestor/detalhamento.jsp");
	
		}
		catch(Exception e){
			e.printStackTrace();
			session.setAttribute("error", "Sala n�o cadastrada, entre em contato");
			response.sendRedirect("pages/erro.jsp");
		}
	}

}
