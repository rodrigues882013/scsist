package controller;

import java.io.IOException;
import java.sql.Timestamp;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.dao.DemonstrativoDAO;
import model.dao.DispositivoDAO;
import model.dao.SalaDAO;
import model.dao.UsuarioDAO;
import model.objects.Demonstrativo;
import model.objects.Sala;
import model.objects.Usuario;
import com.google.gson.Gson;
import communication.ArduinoCOM;


public class AlterarEstados extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	public AlterarEstados() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try{

			String numSala = (String) request.getParameter("sala"); //Numero da Sala
			String estado = (String) request.getParameter("estado"); //
			String idUsuario = (String) request.getParameter("login"); //Login
			String grupo = (String) request.getParameter("grupo"); //Frente, atras e meio
			String json;
			Integer state = 0;
			String novoEstado = "";

			//Recupera a sala, bem como o ultimo usuario que a controlou
			Sala sala = SalaDAO.selectByID(Integer.parseInt(numSala));
			//Recupera o usuario que controlara a sala nesse momento e o define no banco
			Usuario u = UsuarioDAO.selectById(idUsuario);
			sala.setUsuario(u);
			SalaDAO.update(sala);
			boolean condicao = true;
			ArduinoCOM client = ArduinoCOM.getClient(sala.getIp());

			switch(grupo){
				case "3":
					for (int i=0; i<3; i++){
						processaEstado(i, sala, u, condicao);	
					}
					novoEstado = alteraEstado(client, estado, grupo);
					break;
					
				case "6":
					for (int i=3; i<6; i++){
						processaEstado(i, sala, u, condicao);	
					}
					novoEstado = alteraEstado(client, estado, grupo);
					break;
				
				case "9":
					for (int i=6; i<9; i++){
						processaEstado(i, sala, u, condicao);	
					}
					novoEstado = alteraEstado(client, estado, grupo);
					break;
				
				case "Ar":
					for (int i=9; i<11; i++){
						processaEstado(i, sala, u, condicao);	
					}
					novoEstado = alteraEstado(client, estado, grupo);
					break;
					
				case "Datashow":
					for (int i=11; i<12; i++){
						processaEstado(i, sala, u, condicao);	
					}
					novoEstado = alteraEstado(client, estado, grupo);
					break;
			}
			
			json = new Gson().toJson(novoEstado);
			response.setContentType("application/json"); 
			response.setCharacterEncoding("utf-8"); 
			response.getWriter().write(json);

		}
		catch(Exception e){
			e.printStackTrace();
			session.setAttribute("erro", "Erro, tente de novo ou contacte a administração");
			response.sendRedirect("pages/erro.jsp");
		}
	}
	
	protected void processaEstado(int i, Sala sala,  Usuario u, boolean condicao){
		try{
			if (i < sala.getDispositivos().size()){ //Checa se ha pelo menos um dispositivo
				sala.getDispositivos().get(i).setEstado(); //Altera o estado na aplicação
				Demonstrativo d = new Demonstrativo();
				d.setSala(sala);
				d.setDipositivo(sala.getDispositivos().get(i));
				d.setUsuario(u);
				if(sala.getDispositivos().get(i).getEstado().toString().compareTo("LIGADO") == 0){
					d.setTempoInicio(new Timestamp(System.currentTimeMillis()).toString());
					condicao = DemonstrativoDAO.insert(d);
				}
				else{
					if(sala.getDispositivos().get(i).getEstado().toString().compareTo("DESLIGADO") == 0){
						d.setTempoFim(new Timestamp(System.currentTimeMillis()).toString());
						condicao = DemonstrativoDAO.update(d);
					}
				}
				DispositivoDAO.update(sala.getDispositivos().get(i).getNumero().toString(), sala.getDispositivos().get(i).getEstado().toString());
			}

		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	protected String alteraEstado(ArduinoCOM client, String state, String grupo){
		try{
			//Vai passar o estado do grupo
			int estado;
			String novoEstado = "";
			estado = client.changeState(state, grupo); //Passa o estado para o o communication.Client alterar o estado

			if (estado == 1)novoEstado = "LIGADO";
			if (estado == 0)novoEstado = "DESLIGADO";
			if (estado == -1)novoEstado = "FALHA";

			client.close();

			return novoEstado;
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
