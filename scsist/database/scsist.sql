-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 18, 2013 at 12:24 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `scsist`
--

-- --------------------------------------------------------

--
-- Table structure for table `demonstrativo`
--

CREATE TABLE IF NOT EXISTS `demonstrativo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tempoInicio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tempoFim` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_dispositivo` int(11) NOT NULL,
  `id_sala` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_dispositivo` (`id_dispositivo`),
  KEY `id_sala` (`id_sala`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=105 ;

--
-- Dumping data for table `demonstrativo`
--

INSERT INTO `demonstrativo` (`id`, `tempoInicio`, `tempoFim`, `id_dispositivo`, `id_sala`) VALUES
(1, '2013-02-23 13:08:15', '2013-02-23 15:12:15', 2, 2),
(2, '2013-01-27 12:08:15', '2013-01-27 14:12:15', 2, 2),
(3, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 1, 1),
(5, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 1, 1),
(6, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 1, 1),
(7, '2013-04-27 16:53:28', '2013-04-27 17:43:28', 2, 2),
(8, '2013-05-20 16:53:28', '2013-05-20 18:53:28', 2, 2),
(9, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 1, 1),
(10, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 1, 1),
(11, '2013-04-27 16:53:28', '2013-04-27 19:53:28', 3, 3),
(12, '2013-05-05 12:53:28', '2013-05-05 16:53:28', 3, 3),
(13, '2013-02-27 16:53:28', '2013-02-27 18:13:28', 3, 3),
(14, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(15, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(16, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(17, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(18, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(19, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(20, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(21, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(22, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(23, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(24, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(25, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(26, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(27, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(28, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(29, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(30, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(31, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(32, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(33, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(34, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(35, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(36, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(37, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(38, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(39, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(40, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(41, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(42, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(43, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(44, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(45, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(46, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(47, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(48, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(49, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(50, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(51, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(52, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(53, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(54, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(55, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(56, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(57, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(58, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(59, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(60, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(61, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(62, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(63, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(64, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(65, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(66, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(67, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(68, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(69, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(70, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(71, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(72, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(73, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(74, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(75, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(76, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(77, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(78, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(79, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(80, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(81, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(82, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(83, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(84, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(85, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(86, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(87, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(88, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(89, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(90, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(91, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(92, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(93, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(94, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(95, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(96, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(97, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(98, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(99, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(100, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(101, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(102, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(103, '2013-08-22 13:25:44', '2013-08-22 13:25:44', 4, 1),
(104, '2013-08-22 13:25:44', '0000-00-00 00:00:00', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dispositivo`
--

CREATE TABLE IF NOT EXISTS `dispositivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indentificador` int(11) NOT NULL,
  `potencia` float NOT NULL,
  `tipo` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `id_sala` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo` (`tipo`),
  KEY `estado` (`estado`),
  KEY `fk_sala_dispositivo` (`id_sala`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `dispositivo`
--

INSERT INTO `dispositivo` (`id`, `indentificador`, `potencia`, `tipo`, `estado`, `id_sala`) VALUES
(1, 1, 60, 1, 1, 1),
(2, 2, 120, 1, 1, 2),
(3, 3, 60, 1, 1, 3),
(4, 4, 60, 1, 2, 1),
(5, 0, 60, 2, 2, 5),
(6, 1, 60, 1, 1, 5),
(7, 2, 60, 3, 2, 5),
(8, 3, 60, 3, 2, 5),
(9, 4, 60, 1, 2, 5),
(10, 5, 60, 1, 2, 5),
(11, 6, 60, 1, 2, 5),
(12, 7, 60, 1, 2, 5),
(13, 8, 60, 1, 2, 5),
(14, 9, 60, 1, 2, 5),
(15, 10, 60, 1, 2, 5),
(16, 11, 60, 1, 2, 5),
(17, 0, 40, 1, 2, 6),
(18, 1, 40, 1, 1, 6),
(19, 2, 40, 1, 2, 6),
(20, 3, 40, 1, 2, 6),
(21, 0, 40, 1, 2, 7),
(22, 1, 40, 1, 2, 7),
(23, 2, 40, 1, 2, 7),
(24, 3, 40, 1, 2, 7),
(25, 4, 5000, 3, 2, 7),
(26, 0, 60, 1, 2, 8),
(27, 1, 60, 1, 2, 8),
(28, 2, 60, 1, 2, 8),
(29, 3, 5000, 3, 2, 8),
(30, 4, 20, 2, 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `estado`
--

CREATE TABLE IF NOT EXISTS `estado` (
  `id` int(11) NOT NULL,
  `estado` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estado`
--

INSERT INTO `estado` (`id`, `estado`) VALUES
(1, 'LIGADO'),
(2, 'DESLIGADO'),
(3, 'FALHA');

-- --------------------------------------------------------

--
-- Table structure for table `nivel`
--

CREATE TABLE IF NOT EXISTS `nivel` (
  `id` int(11) NOT NULL,
  `nivel` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nivel`
--

INSERT INTO `nivel` (`id`, `nivel`) VALUES
(1, 'GESTOR'),
(2, 'SUPERVISOR'),
(3, 'PROFESSOR');

-- --------------------------------------------------------

--
-- Stand-in structure for view `relatorio_dois`
--
CREATE TABLE IF NOT EXISTS `relatorio_dois` (
`Sala` int(11)
,`Dia` date
,`Nome` varchar(60)
,`Login` varchar(20)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `relatorio_quatro`
--
CREATE TABLE IF NOT EXISTS `relatorio_quatro` (
`Sala` int(11)
,`Tempo_funcionando` decimal(46,4)
,`Consumo` double
,`Periodo_Inicial` timestamp
,`Periodo_Final` timestamp
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `relatorio_tres`
--
CREATE TABLE IF NOT EXISTS `relatorio_tres` (
`Mes` int(2)
,`Sala` int(11)
,`Tempo_funcionando` decimal(46,4)
,`Consumo` double
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `relatorio_um`
--
CREATE TABLE IF NOT EXISTS `relatorio_um` (
`Sala` int(11)
,`Tempo_funcionando` decimal(46,4)
,`Consumo` double
);
-- --------------------------------------------------------

--
-- Table structure for table `sala`
--

CREATE TABLE IF NOT EXISTS `sala` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `mac` varchar(19) NOT NULL,
  `id_usuario` varchar(20) DEFAULT NULL,
  `uso` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `sala`
--

INSERT INTO `sala` (`id`, `numero`, `ip`, `mac`, `id_usuario`, `uso`) VALUES
(1, 1, '10.1.1.18', '10.1.1.18', 'root', 0),
(2, 2, '10.1.1.19', '10.1.1.19', 'root', 0),
(3, 3, '10.1.1.20', '10.1.1.20', 'root', 0),
(4, 4, '10.1.1.21', '10.1.1.21', 'root', 0),
(5, 5, '10.1.1.22', '10.1.1.22', 'root', 0),
(6, 6, '10.1.1.23', '10.1.1.23', 'root', 0),
(7, 14, '10.1.1.87', '10.1.1.87', NULL, NULL),
(8, 34, '10.1.1.98', '10.1.1.98', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tipos`
--

CREATE TABLE IF NOT EXISTS `tipos` (
  `id` int(11) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipos`
--

INSERT INTO `tipos` (`id`, `tipo`) VALUES
(1, 'ILUMINACAO'),
(2, 'DATASHOW'),
(3, 'AR');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `senha` varchar(80) DEFAULT NULL,
  `nivel` int(11) NOT NULL,
  `login` varchar(20) NOT NULL,
  PRIMARY KEY (`login`),
  KEY `nivel` (`nivel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `senha`, `nivel`, `login`) VALUES
(2, 'Felipe', '8450DD1D84F463FEF54F0E11212380F98534727B1D8A357165BC6239C71FFAC1', 2, 'lipe'),
(4, 'Altemar', '5ECD1A94DA3F6C7F00A0436BF0E67E83829798886EEB2B9D85CBCC2A52ACB436', 1, 'mazin'),
(3, 'Natalia Cristina da Silva Lopes', '2255FA204A7F96FB20F5FC8986DFC97CA36A4208C8847CC3758E171A26780937', 3, 'naty'),
(1, 'root', '4813494D137E1631BBA301D5ACAB6E7BB7AA74CE1185D456565EF51D737677B2', 1, 'root');

-- --------------------------------------------------------

--
-- Structure for view `relatorio_dois`
--
DROP TABLE IF EXISTS `relatorio_dois`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `relatorio_dois` AS select `sala`.`numero` AS `Sala`,cast(`demonstrativo`.`tempoInicio` as date) AS `Dia`,`usuario`.`nome` AS `Nome`,`usuario`.`login` AS `Login` from ((`sala` join `usuario` on((`sala`.`id_usuario` = `usuario`.`login`))) join `demonstrativo` on((`demonstrativo`.`id_sala` = `sala`.`id`))) group by `sala`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `relatorio_quatro`
--
DROP TABLE IF EXISTS `relatorio_quatro`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `relatorio_quatro` AS select `demonstrativo`.`id_sala` AS `Sala`,sum((timestampdiff(SECOND,`demonstrativo`.`tempoInicio`,`demonstrativo`.`tempoFim`) / 3600)) AS `Tempo_funcionando`,sum((`dispositivo`.`potencia` * (timestampdiff(SECOND,`demonstrativo`.`tempoInicio`,`demonstrativo`.`tempoFim`) / 3600))) AS `Consumo`,`demonstrativo`.`tempoInicio` AS `Periodo_Inicial`,`demonstrativo`.`tempoFim` AS `Periodo_Final` from (`demonstrativo` join `dispositivo` on((`demonstrativo`.`id_dispositivo` = `dispositivo`.`id`))) group by `demonstrativo`.`tempoInicio`;

-- --------------------------------------------------------

--
-- Structure for view `relatorio_tres`
--
DROP TABLE IF EXISTS `relatorio_tres`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `relatorio_tres` AS select month(cast(`demonstrativo`.`tempoInicio` as date)) AS `Mes`,`demonstrativo`.`id_sala` AS `Sala`,sum((timestampdiff(SECOND,`demonstrativo`.`tempoInicio`,`demonstrativo`.`tempoFim`) / 3600)) AS `Tempo_funcionando`,sum((`dispositivo`.`potencia` * (timestampdiff(SECOND,`demonstrativo`.`tempoInicio`,`demonstrativo`.`tempoFim`) / 3600))) AS `Consumo` from (`demonstrativo` join `dispositivo` on((`demonstrativo`.`id_dispositivo` = `dispositivo`.`id`))) where ((month(cast(`demonstrativo`.`tempoInicio` as date)) between '00' and '13') and (`dispositivo`.`id` = `demonstrativo`.`id_dispositivo`)) group by month(cast(`demonstrativo`.`tempoInicio` as date));

-- --------------------------------------------------------

--
-- Structure for view `relatorio_um`
--
DROP TABLE IF EXISTS `relatorio_um`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `relatorio_um` AS select `demonstrativo`.`id_sala` AS `Sala`,sum((timestampdiff(SECOND,`demonstrativo`.`tempoInicio`,`demonstrativo`.`tempoFim`) / 3600)) AS `Tempo_funcionando`,sum((`dispositivo`.`potencia` * (timestampdiff(SECOND,`demonstrativo`.`tempoInicio`,`demonstrativo`.`tempoFim`) / 3600))) AS `Consumo` from (`demonstrativo` join `dispositivo` on((`demonstrativo`.`id_dispositivo` = `dispositivo`.`id`))) group by `demonstrativo`.`id_sala`;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `demonstrativo`
--
ALTER TABLE `demonstrativo`
  ADD CONSTRAINT `demonstrativo_ibfk_1` FOREIGN KEY (`id_dispositivo`) REFERENCES `dispositivo` (`id`),
  ADD CONSTRAINT `demonstrativo_ibfk_2` FOREIGN KEY (`id_sala`) REFERENCES `sala` (`id`);

--
-- Constraints for table `dispositivo`
--
ALTER TABLE `dispositivo`
  ADD CONSTRAINT `dispositivo_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `tipos` (`id`),
  ADD CONSTRAINT `dispositivo_ibfk_2` FOREIGN KEY (`estado`) REFERENCES `estado` (`id`),
  ADD CONSTRAINT `fk_sala_dispositivo` FOREIGN KEY (`id_sala`) REFERENCES `sala` (`id`);

--
-- Constraints for table `sala`
--
ALTER TABLE `sala`
  ADD CONSTRAINT `sala_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`login`);

--
-- Constraints for table `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`nivel`) REFERENCES `nivel` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
